﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed;

    public Rigidbody rigidbody;

    float horizonatalAxis;
    float verticalAxis;

    private bool IsColliding;

   // public float choppingtime = 3.0f;

    public GameObject vegetable;
    public Transform vegetableBag;
    public int vegetablecount;

    public string vegetable1Id;
    public string vegetable2Id;
    public string vegetable3Id;

    void Start()
    {
        IsColliding = false;
        vegetablecount = 0;

        rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {

      if (GetComponent<ChefDetails>().PlayerScriptableObject.thisPlayerState == PlayerScriptableObject.PLAYER_STATE.MOVING)
      {
          GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;

          // Given specific controls to each player depending on their player selection.
          if (GetComponent<ChefDetails>().PlayerScriptableObject.thisPlayer == PlayerScriptableObject.PLAYER.PLAYER_1)
          {
              horizonatalAxis = Input.GetAxis("HorizontalP1");
              verticalAxis = Input.GetAxis("VerticalP1");
          }
          else
          {
              horizonatalAxis = Input.GetAxis("HorizontalP2");
              verticalAxis = Input.GetAxis("VerticalP2");
          }
      }
//        if (Input.GetKey(left))
//        else if (Input.GetKey(right))
//        else if (Input.GetKey(up))
//            rigidbody.velocity = new Vector3(rigidbody.velocity.x, 0.0f, -moveSpeed);
//        else if (Input.GetKey(down))
//            rigidbody.velocity = new Vector3(rigidbody.velocity.x, 0.0f, moveSpeed);

        if (IsColliding)
        {
           if (vegetablecount < 2)
            {              

              switch (other.gameObject.GetComponent<ObjectType>().objectType)
              {
                  case ObjectType.TYPE.VEGETABLE:
                      {
                          if (Input.GetKeyDown(KeyCode.E))
                          {
                              addVegetableToList(other.name);
                          }
                          break;
                      }
                    }
              //  if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.L))
              //  {
                  //  GameObject GO = Instantiate(vegetable, vegetableBag.position, vegetableBag.rotation);
                  //  GO.transform.SetParent(this.transform);
                //    vegetablecount++;
            //    }

            //   vegetableBag.position = new Vector3(transform.position.x + 1.0f, transform.position.y, transform.position.z);
            }

        }
    }
    private void FixedUpdate()
    {
        Vector3 tempVect = new Vector3(horizonatalAxis, verticalAxis, 0);
        tempVect = tempVect.normalized * moveSpeed * Time.deltaTime;
        rigidbody.MovePosition(rigidbody.transform.position + tempVect);
    }

    public void OnTriggerEnter(Collider other)
    {
        IsColliding = true;
    }

    public void OnTriggerExit(Collider other)
    {
        IsColliding = false;
    }

    public void addVegetableToList(string vegetable)
    {
        if (GetComponent<ChefDetails>().pickedVegetables.Count < 2)
        {
            if (!GetComponent<ChefDetails>().pickedVegetables.Contains(vegetable))
            {
                GetComponent<ChefDetails>().pickedVegetables.Add(vegetable);
                GetComponent<ChefDetails>().assignVegetablesToChef();
            }
        }
        else
        {
            Debug.Log("Only 2 vegetables are allowed");
        }
    }
