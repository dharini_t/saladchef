﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectType : MonoBehaviour
{

    public enum TYPE
    {
        VEGETABLE,
        PICKUP
    }

     public TYPE objectType;

     public PlayerDetails player;
}
