﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/SpawnManagerScriptableObject", order = 1)]


public class PlayerScriptableObject : ScriptableObject
{
    //enum to hold player

    public enum player
    {
        PLAYER_1,
        PLAYER_2
    }

    public enum PLAYER_STATE
    {
        CHOPPING,
        MOVING
    }

    public PLAYER thisPlayer;
    public PLAYER_STATE thisPlayerState;
}
