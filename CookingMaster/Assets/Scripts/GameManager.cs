﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
  public static GameManager Instance;

  public GameObject player1;
  public GameObject player2;

  public List<GameObject> chefTableObjectsList;


  private void Awake()
  {
      Instance = this;
  }

    // Start is called before the first frame update
    void Start()
    {
      player1 = GameObject.Find("Player1");
      player2 = GameObject.Find("Player2");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
